const socket = io();

// ?? Elements
let $form = document.querySelector('.form');
let $messages = document.querySelector('#messages');

// ?? Template
let messageTemplate = document.querySelector('#message-template').innerHTML;
let locationTemplate = document.querySelector('#location-template').innerHTML;
let sidebarTemplate = document.querySelector('#sidebar-template').innerHTML;

// ?? Options
const { username, room } = Qs.parse(location.search, {
  ignoreQueryPrefix: true,
});

// ?? Autoscroll
const autoscroll = () => {
  // ** new message element
  const $newMessage = $messages.lastElementChild;
  //** Height of new element
  const newMessageStyles = getComputedStyle($newMessage);
  const newMessageMargin = parseInt(newMessageStyles.marginBottom);
  const newMessageHeight = $newMessage.offsetHeight + newMessageMargin;

  // ** Visible height
  const visibleHeight = $messages.offsetHeight;

  // ** Height of messages container
  const containerHeight = $messages.scrollHeight;

  // ** How far i scroll?
  const scrollOffset = $messages.scrollTop + visibleHeight;

  if (containerHeight - newMessageHeight <= scrollOffset) {
    $messages.scrollTop = $messages.scrollHeight;
  }
};

socket.on('message', message => {
  const html = Mustache.render(messageTemplate, {
    username: message.username || 'User',
    message: message.text,
    createdAt: moment(message.createdAt).format('h:mm a'),
  });
  $messages.insertAdjacentHTML('beforeend', html);
  autoscroll();
});

$form.addEventListener('submit', e => {
  e.preventDefault();

  let message = e.target.elements.messInput.value;
  socket.emit('sendMessage', message, error => {
    if (error) return console.log(`Error: ${error}`);
  });

  e.target.elements.messInput.value = '';
});

let locationBtn = document.querySelector('.location-btn');

socket.on('locationMessage', location => {
  const html = Mustache.render(locationTemplate, {
    username: location.username || 'User',
    location: location.text,
    createdAt: moment(location.createdAt).format('h:mm a'),
  });
  $messages.insertAdjacentHTML('beforeend', html);
  autoscroll();
});

socket.on('roomData', ({ room, users }) => {
  console.log(room);
  console.log(users);

  const html = Mustache.render(sidebarTemplate, {
    room,
    users,
  });
  document.querySelector('#sidebar').innerHTML = html;
});

locationBtn.addEventListener('click', e => {
  if (!navigator.geolocation) return alert('geolocation not supported by this browser');
  e.target.setAttribute('disabled', 'disabled');
  navigator.geolocation.getCurrentPosition(position => {
    let { latitude: lat, longitude: long } = position.coords;
    socket.emit('sendLocation', { lat, long }, msg => {
      e.target.removeAttribute('disabled');
    });
  });
});

socket.emit('join', { username, room }, error => {
  if (error) {
    alert(error);
    location.href = '/';
  }
});
