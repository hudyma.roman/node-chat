const express = require('express');
const http = require('http');
const path = require('path');
const socketio = require('socket.io');
const Filter = require('bad-words');

// ** Utils
const { generateMessage } = require('./utils/messages');
const { addUser, removeUser, getUser, getUsersInRoom } = require('./utils/users');

const PORT = process.env.PORT || 4000;
const publicPath = path.join(__dirname, 'public');
const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(express.static(publicPath));

let count = 0;

io.on('connection', socket => {
  socket.on('sendMessage', (message, callback) => {
    const user = getUser(socket.id);
    const filter = new Filter();

    if (filter.isProfane(message)) {
      return callback('Profanity is not allowed!');
    }

    io.to(user.room).emit('message', generateMessage(message, user.username));
    callback();
  });

  socket.on('disconnect', () => {
    const user = removeUser(socket.id);

    if (user) {
      io.to(user.room).emit('message', generateMessage(`${user.username} has disconnect`, 'Admin'));
      io.to(user.room).emit('roomData', {
        room: user.room,
        users: getUsersInRoom(user.room),
      });
    }
  });

  socket.on('sendLocation', ({ lat, long }, callback) => {
    const user = getUser(socket.id);
    io.to(user.room).emit('locationMessage', generateMessage(`https://www.google.com/maps?q=${lat},${long},`, user.username));
    callback('Location Shared!!!');
  });

  // ?? Join to room

  socket.on('join', (options, callback) => {
    const { error, user } = addUser({ id: socket.id, ...options });
    if (error) return callback(error);

    socket.join(user.room);

    socket.emit('message', generateMessage(`Welcome ${user.username}`, 'Admin'));
    socket.broadcast.to(user.room).emit('message', generateMessage(`${user.username} has in ${user.room} room joined!`, 'Admin'));
    io.to(user.room).emit('roomData', {
      room: user.room,
      users: getUsersInRoom(user.room),
    });

    callback();
  });
});

app.get('', (req, res) => {
  res.sendFile(publicPath + 'index.html');
});

server.listen(PORT, () => {
  console.log('App listening on port 4000!');
});
